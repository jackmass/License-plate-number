import Taro, { } from '@tarojs/taro'
import { View, Text, Input, Button } from '@tarojs/components'
import Custom_car_number_input from "./custom-car-number-input"
import Custom_keyboard from "./custom-keyboard"
import { useEffect, useRef, useState, forwardRef, useImperativeHandle, useMemo } from 'react'
function CarPlateInput(props, ref) {
    const customKeyboardRef = useRef(null)
    const customCarNumInputRef = useRef(null)
    const [customCarNumInputIndex, setCustomCarNumInputIndex] = useState(0)
    const carNumber = useMemo(() => {
        return (customCarNumInputRef?.current?.numbers || []).map(item => item.value).join('')
    }, [customCarNumInputRef?.current?.numbers])

 

  

    const changeKerboard = (type) => {
        if (customKeyboardRef?.current) {
            customKeyboardRef.current.change(type);
            customKeyboardRef.current.shows();
        }

    }

    const onInputFocus = (index) => {

        if (index == 0) {
            changeKerboard(1);
        } else if (index == 7) {
            changeKerboard(3);
        } else {
            changeKerboard(2);
        }
        setCustomCarNumInputIndex(index)

    }

    const onKeyboardClick = (e) => {
        customCarNumInputRef.current.change({
            index: customCarNumInputIndex,
            value: e,
            remove: false
        })
    }

    const onKeyboardDelete = (e) => {
        customCarNumInputRef.current.change({
            index: customCarNumInputIndex,
            value: '',
            remove: true
        })
    }



    useImperativeHandle(ref, () => {
        return {
            carNumber
        }
    })

    return (
        <View style={{ paddinTop: '60rpx', margin: "40rpx 0", background: "#fff" }}>
            <Custom_car_number_input ref={customCarNumInputRef} bindfocus={onInputFocus}></Custom_car_number_input>
            <Custom_keyboard ref={customKeyboardRef} keyboardType={1} bindclick={onKeyboardClick} binddelete={onKeyboardDelete}></Custom_keyboard>
        </View>
    )
}

export default forwardRef(CarPlateInput) 
