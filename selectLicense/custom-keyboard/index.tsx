import Taro, { } from '@tarojs/taro'
import { View, Text, Input, Button, Image } from '@tarojs/components'
import { useEffect, useRef, useState, forwardRef, useImperativeHandle } from 'react'
import styles from "./index.module.scss"
const CustomKeyboard = ({ keyboardType, binddelete, bindclick }, ref) => {

  const resourcesUrl = Taro.getStorageSync("resourcesUrl");
  const [keyboard, setKeyboard] = useState<any[]>([])
  const [show, setShow] = useState<boolean>(false)
  const [provinces] = useState([
    ['京', '津', '沪', '渝', '冀', '豫', '云', '辽', '黑'],
    ['湘', '皖', '鲁', '新', '苏', '浙', '赣', '鄂', '桂'],
    ['甘', '晋', '蒙', '陕', '吉', '闽', '贵', '粤', '青'],
    ['藏', '川', '宁', '琼', '使', '无', {
      value: '',
      empty: true
    }, {
        value: '',
        empty: true
      }, {
        value: '',
        backspace: true
      }],
  ])
  const [nomals] = useState([
    ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'],
    ['Q', 'W', 'E', 'R', 'T', 'Y', 'U', {
      value: 'O',
      disabled: true
    }, 'P', '港'],
    ['A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', '澳'],
    ['Z', 'X', 'C', 'V', 'B', 'N', 'M', '学', '领', {
      value: '',
      backspace: true
    }],
  ])

  const [ecos] = useState([
    ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'],
    ['Q', 'W', 'E', 'R', 'T', 'Y', 'U', {
      value: 'O',
      disabled: true
    }, 'P', {
        value: '港',
        disabled: true
      }],
    ['A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', {
      value: '澳',
      disabled: true
    }],
    ['Z', 'X', 'C', 'V', 'B', 'N', 'M', {
      value: '学',
      disabled: true
    }, {
        value: '领',
        disabled: true
      }, {
        value: '',
        backspace: true
      }],
  ])

  // useEffect(() => {
  //   change(keyboardType);
  // }, [])
  

  const change = (keyboardType) => {
    console.log("keyboardType",keyboardType);
    
    switch (parseInt(keyboardType)) {
      case 1:
        setKeyboard([...provinces])
        break;
      case 2:
        setKeyboard([...nomals])
        break;
      case 3:
        setKeyboard([...ecos])
        break;
    }
  }

  const shows = () => {
    if (!show) {

      setShow(true)
    }
  }

  const hide = (e) => {
    setShow(false)
  }

  const deletes = () => {
    binddelete()
  }

  const click = (value) => {
    bindclick(value)
  }

  useImperativeHandle(ref, () => {
    return {
      shows,
      change
    }
  })

  return (
    <View className={`${styles.custom_keyboard} ${show ? styles.show : styles.hide}`}>

      <View className={styles.toolbar}>
        <View className={styles.complete} onClick={hide}>完成</View>
      </View>
      <View className={styles.keyboard}>
        {
          (keyboard || []).map((rows, rowIndex) => (
            <View className={styles.row} key={rowIndex}>
          
              {
                (rows || []).map(col => (
                  < >
                   
                    {
                      col?.disabled ? (<View className={`${styles.col} ${styles.disabled}`}>{col.value}</View>) : col?.empty ? (<View className={`${styles.col} ${styles.empty}`}>{col.value}</View>) : col?.backspace ? (<View className={`${styles.col} ${styles.backspace}`} onClick={() => deletes()}>
                        <Image className={styles.icon} src={resourcesUrl + '/icon/backspace.png'}></Image>
                      </View>) : (<View className={styles.col} onClick={() => click(col)} >{col}</View>)
                    }



                  </>
                ))
              }
            </View>
          ))
        }

      </View>
      <View className={styles.footer}></View>
    </View>
  )
}

export default forwardRef(CustomKeyboard) 
